package parser

import (
	"regexp"
	"strings"
)

type Import struct {
	Alias string
	Path  string
}

type Imports []Import

func findImports(buffer []byte) Imports {
	mlir := regexp.MustCompile(`(?s)import \([^()]+\)`)
	inmlir := regexp.MustCompile(`(?P<alias>\w*)* *"(?P<path>.*)"`)

	olir := regexp.MustCompile(`import (?P<alias>\w*)* *"(?P<path>.*)"`)

	imps := make(Imports, 0)

	sbuffer := make([][][]byte, 0)

	ml := mlir.FindAll(buffer, -1)
	for _, bytes := range ml {
		sbuffer = append(sbuffer, inmlir.FindAllSubmatch(bytes, -1)...)
	}

	sbuffer = append(sbuffer, olir.FindAllSubmatch(buffer, -1)...)

	for _, d := range sbuffer {
		imp := Import{}

		for i, regexpGroupName := range inmlir.SubexpNames() {
			if regexpGroupName == "alias" {
				imp.Alias = string(d[i])
			}

			if regexpGroupName == "path" {
				imp.Path = string(d[i])
			}
		}

		imps = append(imps, imp)
	}

	return imps
}

func ParseNestedImports(buffer []byte, fields Fields) Imports {
	imports := findImports(buffer)

	imps := Imports{}
	mi := make(map[string]Import)

	for _, f := range fields {
		p, e := f.Package()
		if e {
			for _, i := range imports {
				if i.Alias == p || strings.Contains(i.Path, p) {
					mi[i.Path] = i
				}
			}
		}
	}

	for _, i := range mi {
		imps = append(imps, i)
	}

	return imps
}
