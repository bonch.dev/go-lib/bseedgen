package parser

import (
	"fmt"
	"regexp"
)

func ParseStructFields(buffer []byte, name string) Fields {
	r, err := regexp.Compile(fmt.Sprintf("(?s)type %s struct {[^{}]+}", name))
	if err != nil {
		panic(err)
	}

	return parseStruct(r.Find(buffer))
}

func parseStruct(structBytes []byte) Fields {
	c := regexp.MustCompile(`\t(?P<name>[A-Z]\w+) +(?P<type>[\w.*]+)`)

	m := make(Fields, 0)

	for _, d := range c.FindAllSubmatch(structBytes, -1) {
		f := Field{}

		for i, regexpGroupName := range c.SubexpNames() {
			if regexpGroupName == "name" {
				f.Name = string(d[i])
			}

			if regexpGroupName == "type" {
				f.Type = string(d[i])
			}
		}

		if f.Name == "R" || f.Name == "L" {
			continue
		}

		m = append(m, f)
	}

	return m
}

type Field struct {
	Name string
	Type string
}

func (f *Field) Package() (p string, exists bool) {
	c := regexp.MustCompile(`^(.*)\.(.*)$`)
	s := c.FindAllStringSubmatch(f.Type, -1)

	if s == nil {
		return "", false
	}

	return s[0][1], true
}

type Fields []Field
