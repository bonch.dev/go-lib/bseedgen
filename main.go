package main

import (
	"gitlab.com/bonch.dev/go-lib/bseedgen/cmd"
)

func main() {
	if err := cmd.DefaultCommand().Execute(); err != nil {
		panic(err)
	}
}
