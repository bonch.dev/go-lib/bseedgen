package cmd

import "gitlab.com/bonch.dev/go-lib/bseedgen/parser"

type Config struct {
	Package      string
	Model        string
	ModelPackage string
	Imports      parser.Imports
	Fields       parser.Fields
}
