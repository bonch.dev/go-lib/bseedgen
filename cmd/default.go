package cmd

import (
	"bytes"
	"fmt"
	"go/format"
	goparser "go/parser"
	"go/token"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"text/template"
	"unicode/utf8"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/bonch.dev/go-lib/bseedgen/parser"
	"gitlab.com/bonch.dev/go-lib/bseedgen/templates"
)

var (
	model        string
	modelPackage string
	output       string
	input        string
)

func DefaultCommand() *cobra.Command {
	defaultCommand := &cobra.Command{
		Use:   "bseedgen",
		Short: "BSeedGen CLI is a helper to create own seeders",
		Long:  "BSeedGen CLI is a helper to create own seeders, based on sqlboiler models",
		RunE: func(cmd *cobra.Command, args []string) error {
			destination, pack := parseFromOutput(model, output)

			modelFileData, err := loadFile(input)
			if err != nil {
				return errors.Wrap(err, "loading file error")
			}

			if modelPackage == "" {
				c := exec.Command("go", "list", "-f", "'{{.ImportPath}}'", "./...")
				data, err := c.Output()
				if err != nil {
					return err
				}

				exploded := explodePath(input)

				for _, ps := range strings.Split(string(data), "\n") {
					if strings.Contains(ps, relativePath(exploded[:len(exploded)-1])) {
						modelPackage = strings.ReplaceAll(ps, "'", "")

						break
					}
				}

				if modelPackage == "" {
					return errors.New("model package not found: probably, not project root folder")
				}
			}

			cfg := Config{}

			cfg.Model = model
			cfg.Package = pack
			cfg.ModelPackage = modelPackage

			cfg.Fields = parser.ParseStructFields(modelFileData, cfg.Model)
			cfg.Imports = parser.ParseNestedImports(modelFileData, cfg.Fields)

			t := template.Must(template.New("seeder").Parse(templates.Seeder))

			buf := bytes.Buffer{}
			if err := t.Execute(&buf, &cfg); err != nil {
				return errors.Wrap(err, "template executing error")
			}

			bs, err := format.Source(buf.Bytes())
			if err != nil {
				return errors.Wrap(err, "gofmt error")
			}

			if err := writeFile(destination, bs); err != nil {
				return errors.Wrap(err, "write file error")
			}

			return nil
		},
	}

	defaultCommand.Flags().StringVarP(&model, "model", "m", "", "Parsing model")
	defaultCommand.Flags().StringVarP(&modelPackage, "model-package", "p", "", "Parsing model package")
	defaultCommand.Flags().StringVarP(&input, "input", "i", "", "Parsing model path")
	defaultCommand.Flags().StringVarP(&output, "output", "o", "", "Output directory for seeder")

	defaultCommand.MarkFlagRequired("model")
	defaultCommand.MarkFlagFilename("input")
	defaultCommand.MarkFlagDirname("output")

	return defaultCommand
}

func loadFile(file string) ([]byte, error) {
	return ioutil.ReadFile(file)
}

func relativePath(inputPath []string) string {
	if len(inputPath) == 0 {
		return ""
	}

	if len(inputPath) == 1 {
		return inputPath[0]
	}

	if inputPath[0] == "." {
		inputPath = inputPath[1:]
	}

	return strings.Join(inputPath, "/")
}

func explodePath(inputPath string) []string {
	exploded := strings.Split(inputPath, "/")
	path := make([]string, 0)

	for _, s := range exploded {
		if utf8.RuneCountInString(s) > 0 {
			path = append(path, s)
		}
	}

	return path
}

func parseFromOutput(modelName, output string) (destination, pack string) {
	path := explodePath(output)

	if output != "." {
		pack = path[len(path)-1]
	} else {
		pack = "main"
	}

	path = append(path, fmt.Sprintf("%s.go", strings.ToLower(modelName)))

	destination = strings.Join(path, "/")

	return
}

func writeFile(output string, data []byte) error {
	file, err := os.OpenFile(output, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		return err
	}

	defer func() {
		if err := file.Close(); err != nil {
			panic(err)
		}
	}()

	if _, err := file.Write(data); err != nil {
		return err
	}

	return nil
}

func packageName(file string) (string, error) {
	fset := token.NewFileSet()

	// parse the go soure file, but only the package clause
	astFile, err := goparser.ParseFile(fset, file, nil, goparser.PackageClauseOnly)
	if err != nil {
		return "", err
	}

	if astFile.Name == nil {
		return "", fmt.Errorf("no package name found")
	}

	return astFile.Name.Name, nil
}
