package cmd

import "testing"

func TestPackageName(t *testing.T) {
	input := "../example/test2.tgo"

	p, err := packageName(input)
	if err != nil {
		t.Error(err.Error())
	}

	println(p)
}
