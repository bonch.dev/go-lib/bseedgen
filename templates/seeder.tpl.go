package templates

var Seeder = `package {{ .Package }}

import (
	seedmodels "{{ .ModelPackage }}"

	"context"
	"database/sql"

	"github.com/volatiletech/sqlboiler/v4/boil"
{{- range .Imports }}
{{- if .Alias }}
	{{ .Alias }} "{{ .Path }}"
{{- else }}
	"{{ .Path }}"
{{- end }}
{{- end }}
)

type {{ .Model }}Seeder struct {}

func New{{ .Model }}Seeder() *{{ .Model }}Seeder {
	return &{{ .Model }}Seeder{}
}

func (ms *{{ .Model }}Seeder) Up(ctx context.Context, db *sql.DB) error {
	factory := ms.Factory()

	for i := 0; i < 10; i++ {
		model := factory.Definition()
		
		if err := model.Insert(ctx, db, boil.Infer()); err != nil {
			return err
		}
	}
	
	return nil
}

func (ms *{{ .Model }}Seeder) Factory() *{{ .Model }}Factory {
	return &{{ .Model }}Factory{
{{- range .Fields }}
		{{ .Name }}: nil,
{{- end }}
	}
}

type {{ .Model }}Factory struct {
{{- range .Fields }}
    	{{ .Name }} func() {{ .Type }}
{{- end }}
}

func (mf *{{ .Model }}Factory) Definition() seedmodels.{{ .Model }} {
	return seedmodels.{{ .Model }}{
{{- range .Fields }}
		{{ .Name }}: mf.{{ .Name }}(),
{{- end }}
	}
}`
