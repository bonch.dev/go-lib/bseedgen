module gitlab.com/bonch.dev/go-lib/bseedgen

go 1.15

require (
	github.com/pkg/errors v0.8.1
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/cobra v1.1.3
	github.com/volatiletech/null/v8 v8.1.1
	github.com/volatiletech/sqlboiler/v4 v4.4.0
)
